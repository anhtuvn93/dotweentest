﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerController : MonoBehaviour
{
    public TypeTween test;

    void DoMove()
    {
        transform.DOMove(new Vector3(0, 7, 0), 2).From().OnComplete(() =>
        {
            print("Hello World!");
        });
    }

    void DoMoveX()
    {
        transform.DOMoveX(5, 2).From();
    }

    void DoPath()
    {
        Vector3[] points = new Vector3[6] { new Vector3(0, 0, 0), new Vector3(0, 5, 0), new Vector3(2, 3, 0), new Vector3(2, -3, 0), new Vector3(-3, -3, 0), new Vector3(-3, 3, 0), };
        transform.DOPath(points, 5, PathType.CatmullRom).SetOptions(true);
    }

    void DoFade()
    {
        GetComponent<MeshRenderer>().materials[0].DOFade(0, 2).From();
    }

    void DoColor()
    {
        GetComponent<MeshRenderer>().materials[0].DOColor(Color.green, 2).From();
    }

    void Sequence()
    {
        Sequence sequence = DOTween.Sequence();
        sequence.Append(transform.DOMove(new Vector3(0, 7, 0), 2).From());
        sequence.Append(transform.DOMoveX(5, 2).From());
    }

    void Update()
    {
        if (DOTween.TotalPlayingTweens() == 0)
        {
            if (test == TypeTween.DOMove)
            {
                DoMove();
            }
            else if (test == TypeTween.DOMoveX)
            {
                DoMoveX();
            }
            else if (test == TypeTween.DOColor)
            {
                DoColor();
            }
            else if (test == TypeTween.DOFade)
            {
                DoFade();
            }
            else if (test == TypeTween.DOPath)
            {
                DoPath();
            }
            else if (test == TypeTween.Sequence)
            {
                Sequence();
            }
        }
    }
}

public enum TypeTween
{
    DOMove, DOMoveX, DOPath, DOFade, DOColor, Sequence
}
